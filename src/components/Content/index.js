const Content = ({ children }) => {
    return (
        <>
            <header className="text-blue-600">
                This is header of content
            </header>
            <div>{children}</div>
        </>
    )
}

export default Content;