import Sidebar from '@components/Sidebar'
import Content from '@components/Content'

const Layout = ({ sidebar, content }) => {
    return (
        <>
            <Sidebar>{sidebar}</Sidebar>
            <Content>{content}</Content>
        </>
    )
}

export default Layout;