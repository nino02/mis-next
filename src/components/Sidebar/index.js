const Sidebar = ({ children }) => {
    return (
        <>
            <header>This is header for sidebar</header>
            <div>{children}</div>
        </>
    )
}

export default Sidebar;