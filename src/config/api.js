import { getUser } from "@helpers/auth";
import axios from "axios";
import router from "next/router";


const config = (baseURL) => {
  const instance = axios.create({
    baseURL,
    responseType: "json",
    headers: {
      'apiKey': process.env.API_KEY || process.env.NEXT_PUBLIC_API_KEY,
    },
  })

  // Intercept request
  instance.interceptors.request.use(config => {
    const userMIS = getUser();
    config.headers.accessToken = userMIS ? userMIS.accessToken : '';
    return config;
  }, (err) => {
    return Promise.reject(err);
  });

  // Intercept response
  axios.interceptors.response.use(res => {
    if (res.status === 401) {
      router.replace('/auth/login')
    }
    return res;
  }, (err) => {
    return Promise.reject(err);
  });

  return instance;
}

const api = {
  base() {
    const baseURL = process.env.API_BASE_URL || process.env.NEXT_PUBLIC_API_BASE_URL;
    return config(baseURL);
  },
  closing() {
    const baseURL = process.env.API_CLOSING_URL || process.env.NEXT_PUBLIC_API_CLOSING_URL;
    return config(baseURL);
  }
}

export default api;
