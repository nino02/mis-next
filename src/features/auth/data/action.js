import router from 'next/router';
import { v4 as uuidv4 } from 'uuid';

import * as types from './types'
import api from '@config/api'
import localStorage from '@helpers/localStorage';
import { success, failure } from '@helpers/redux'
import { USER_TYPE, USER_ALREADY_LOGOUT, LOGOUT_SUCCESS, LOGIN_SUCCESS } from '@features/auth/constant';
import { getUser } from '@helpers/auth';

export const login = (payload) => async (dispatch) => {
  const resPayload = {
    type: 'web',
    apiKey: process.env.NEXT_PUBLIC_API_KEY
  }
  try {
    const { data } = await api.base().post(`/api/v2/user-mis-login`, { ...payload, ...resPayload });

    dispatch({
      type: success(types.LOGIN),
      payload: data,
    })

    if (data.status === LOGIN_SUCCESS) {
      if (data && data.data) {
        localStorage.set('user-mis', JSON.stringify(data.data))
        localStorage.set('startCountDownRefreshToken', true)
      }
      router.replace('/')
    }
  } catch (err) {
    if (!err.response) return console.error(err)
    dispatch({
      type: failure(types.LOGIN),
      error: err.response.statusText,
    })
  }
}


export const logout = () => async (dispatch, getState) => {
  if (process.env.NEXT_PUBLIC_LOGOUT_TO_GO_CAS) {
    try {
      const { data: userData } = getState().auth;
      const userCookie = getUser();
      const payload = {
        username: userData ? userData.username : userCookie.username,
        token: userData ? userData.accessToken : userCookie.accessToken,
        userType: USER_TYPE,
      }
      const { data } = await api.base().post(`/api/v2/user-mis-logout`, { ...payload });
      dispatch({
        type: success(types.LOGOUT),
        payload: data,
      })

      if (data.status === LOGOUT_SUCCESS) {
        localStorage.remove('user-mis');
        localStorage.remove('startCountDownRefreshToken')
        router.replace('/auth/login')
      }
    } catch (err) {
      if (!err.response) return console.error(err)
      dispatch({
        type: failure(types.LOGOUT),
        error: err.response.statusText,
      })

      if (err.response.status === 400 && err.response.message === USER_ALREADY_LOGOUT) {
        router.replace('/auth/login');
      }
    }
  } else {
    router.replace('/auth/login')
  }
}

export const refreshToken = (setIsRunning) => async (dispatch, getState) => {
  try {
    const user = getUser();
    const { data } = await api.base().get(`/api/v3/refresh?token=${user.accessToken}`, { headers: { "Correlation-id": uuidv4() } });
    
    dispatch({
      type: success(types.LOGOUT),
      payload: data,
    })

    if (data) {
      setIsRunning(true)
    }
  } catch (err) {
    localStorage.remove('startCountDownRefreshToken')
    setIsRunning(false)
    router.replace('/auth/login')
  }
}