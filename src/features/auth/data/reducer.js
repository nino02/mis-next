import * as types from './types'
import { status, success, failure } from "@helpers/redux";

const initialState = {
    data: null,
    status: null,
    error: null,
}

const authReducer = (state = initialState, { type, payload, error }) => {
    switch (type) {
        case success(types.LOGIN):
            return {
                ...state,
                status: status.LOADED,
                data: payload.data,
            }
        case failure(types.LOGIN):
            return {
                ...state,
                status: status.ERROR,
                error
            }
        case success(types.LOGOUT):
            return {
                ...state,
                status: status.LOADED,
                data: payload.data,
            }
        case failure(types.LOGOUT):
            return {
                ...state,
                status: status.ERROR,
                error
            }
        default:
            return state
    }
}

export default authReducer;