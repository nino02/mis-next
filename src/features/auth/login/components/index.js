import Form from './form';

const LoginComponent = () => {
    return (
        <div>
            <h2>LOGIN</h2>
            <Form />
        </div>
    )
}

export default LoginComponent;