import LoginComponent from '@features/auth/login/components'

const LoginContainer = () => {
    return <LoginComponent />
}

export default LoginContainer;