import { css } from '@emotion/css'
import { useSelector } from 'react-redux'

const FOMonitoringContainer = () => {
    const agent = useSelector(state => state.foMonitoring.data)

    return (
        <>
            <div className={css`
                padding: 32px;
                background-color: hotpink;
                font-size: 24px;
                border-radius: 4px;
                &:hover {
                color: 'black';
                }
            `}>try</div>
            <div>{agent && agent.name}</div>
        </>
    )
}

export default FOMonitoringContainer;