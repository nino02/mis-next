import * as types from './types'
import api from '@config/api'
import { success, failure } from '@helpers/redux'

export const tryFetch = () => async (dispatch) => {
  try {
    const branchID = 375;
    const { data } = await api.closing().get(`/api/v1/fo-monitoring/individual-loans/disbursements?branchID=${branchID}`);
    dispatch({
      type: success(types.TRYFETCH),
      payload: data,
    })
  } catch (err) {
    if (!err.response) return console.error(err)
    dispatch({
      type: failure(types.TRYFETCH),
      error: err.response.statusText,
    })
  }
}