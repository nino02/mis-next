import * as types from './types'
import { status, success, failure } from "@helpers/redux";

const initialState = {
    data: null,
    status: null,
    error: null,
}

const agentReducer = (state = initialState, { type, payload, error }) => {
    switch (type) {
        case success(types.TRYFETCH):
            return {
                ...state,
                status: status.LOADED,
                data: payload,
            }
        case failure(types.TRYFETCH):
            return {
                ...state,
                status: status.ERROR,
                error
            }
        default:
            return state
    }
}

export default agentReducer;