import localStorage from "./localStorage"
import { isClient } from "./index";

export const getUser = () => {
    if (!isClient) {
        console.error("Can't access window.localStorage on server")
        return null;
    }

    const userMIS = localStorage.get(process.env.NEXT_PUBLIC_AUTH_PREFIX);
    const userMISParsed = userMIS ? (typeof userMIS === 'object' ? userMIS : JSON.parse(userMIS)) : null;

    return userMISParsed;
}