import { isClient } from "./index";

const storage = isClient ? window.localStorage : null

const set = (key, value) => {
  if (!storage) {
    return false
  }

  return storage.setItem(key, value)
}

const get = (key) => {
  if (!storage) {
    return false
  }

  return storage.getItem(key)
}

const remove = (key) => {
  if (!storage) {
    return false
  }

  return storage.removeItem(key)
}

export default {
  set,
  get,
  remove
}
