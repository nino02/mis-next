import { useRouter } from 'next/router'
import { get } from 'lodash';

import { getUser } from './auth';
import { useEffect, useState } from 'react';

const ProtectRoute = ({ children }) => {
  const router = useRouter();
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true)
  }, [])

  const renderComponent = () => {
    if (mounted) {
      const user = getUser();
      const accessToken = get(user, 'accessToken');
      if (!accessToken && router.pathname !== '/auth/login') {
        return <div>You're not authorized, please login first.</div>
      }
  
      return children;
    }

    return <div>Loading..</div>
  }
  

  return renderComponent();
};

export default ProtectRoute;