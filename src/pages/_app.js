import Head from 'next/head'
import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'

import localStorage from '@helpers/localStorage'
import { wrapper } from '@config/redux-store'
import { refreshToken } from '@features/auth/data/action'
import ProtectRoute from '@helpers/protectedRoute'

import 'tailwindcss/tailwind.css'



const App = ({ Component, pageProps }) => {
  const [isRunning, setIsRunning] = useState(true);
  const dispatch = useDispatch()
  const startCountDownRefreshToken = localStorage.get('startCountDownRefreshToken');

  useEffect(() => {
    let timer = null;
    if (startCountDownRefreshToken && isRunning) {
      timer = setInterval(() => {
        setIsRunning(false);
        dispatch(refreshToken(setIsRunning))
      }, process.env.NEXT_PUBLIC_TIME_TO_REFRESH_TOKEN);
    }
    return () => {
      clearInterval(timer)
    }
  }, [startCountDownRefreshToken, isRunning])

  return (
    <>
      <Head>
        <title>MIS NextJS Version</title>
      </Head>
      <ProtectRoute>
        <Component {...pageProps} />
      </ProtectRoute>
    </>
  )
}

export default wrapper.withRedux(App)
