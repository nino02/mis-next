import { wrapper } from '@config/redux-store'

import { tryFetch } from '@features/fo-monitoring/data/actions'
import FOMonitoringView from '@views/fo-monitoring'

const FOMonitoring = () => {
  return <FOMonitoringView />
}

export const getServerSideProps = wrapper.getServerSideProps((store) => async () => {
  await store.dispatch(tryFetch())
})

export default FOMonitoring;
