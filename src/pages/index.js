import { useDispatch } from 'react-redux';
import { logout } from '@features/auth/data/action'

export default function Home() {
  const dispatch = useDispatch()

  return (
    <div>
      Try
      <h2 onClick={() => dispatch(logout())}>LOGOUT</h2>
    </div>
  )
}
