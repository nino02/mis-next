import { combineReducers } from 'redux'
import AuthReducer from '@features/auth/data/reducer'
import FOMonitoringReducer from '@features/fo-monitoring/data/reducer'

// COMBINED REDUCERS
const reducers = {
  auth: AuthReducer,
  foMonitoring: FOMonitoringReducer,
}

export default combineReducers(reducers)