import LoginContainer from '@features/auth/login/container'

const LoginView = () => {
    return <LoginContainer />
}

export default LoginView;