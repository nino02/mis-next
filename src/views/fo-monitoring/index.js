import FOMonitoringContainer from '@features/fo-monitoring/container'
import Layout from '@components/Layout';

const FOMonitoringView = () => {
    return (
        <Layout content={<FOMonitoringContainer />} />
    )
}

export default FOMonitoringView;